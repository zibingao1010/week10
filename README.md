# Week 10 Mini-Project

First create a folder by running `cargo new week10`. Then fill out `Cargo.toml` and `main.rs` and add `Dockerfile`.

## Dockerize Hugging Face Rust transformer

Build the docker image by running `docker build -t week10 .`.

![image](image1.png)

## Deploy container to AWS Lambda

We can deploy the image to Lambda using ECR.

1. Navigate into the `Identity and Access Management (IAM)` under AWS console. Add a new user and attach necessary polices including 
* `IAMFullAccess`
* `AWSLambda_FullAccess`
* `AWSAppRunnerServicePolicyForECRAccess`
* `EC2InstanceProfileForImageBuilderECRContainerBuilds`

2. Go to `Secruity Credentials` to create new access key.

3. Run `aws configure` to enter the newly created access key.

4. Create a AWS ECR Repo

5. Retrieve an authentication token and authenticate your Docker client to your registry using the AWS CLI:
```
aws ecr get-login-password --region us-east-1 | docker login --username AWS --password-stdin <account number>.dkr.ecr.us-east-1.amazonaws.com
```

6. Tag your image so you can push the image to this repository:
```
docker tag week10:latest <account number>.dkr.ecr.us-east-1.amazonaws.com/week10:latest
```

7. Run the following command to push this image to your newly created AWS repository:
```
docker push <account number>.dkr.ecr.us-east-1.amazonaws.com/week10:latest
```

![image](image2.png)

8. Create the AWS Lambda function and configure it to use the Docker image that we uploaded to ECR.

![image](image3.png)

![image](image4.png)

## Implement query endpoint

Click into your lambda function, and under `configuration`, navigate into the `functional URL`. Create a new function URL with `CORS enabled`.

You can use this command to text:
```
curl -X POST https://g2mhk6jfextx4jxxhfgjfgz6yy0esrpp.lambda-url.us-east-1.on.aws/ -H "Content-Type: application/json" -d '{"text":"hello world"}'
```

![image](image5.png)